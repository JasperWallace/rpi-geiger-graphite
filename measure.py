#!/usr/bin/env python

import time, sys, threading
from datetime import datetime
import RPi.GPIO as GPIO
from graphiteclient import GraphiteClient

# https://github.com/radhoo/uradmonitor_kit1/blob/master/code/misc/detectors.cpp
# http://www.gstube.com/data/2484/

server = port = None

if len(sys.argv) == 3:
  server = sys.argv[1]
  port = int(sys.argv[2])

# 18 in BCM terms
pin = 12

GPIO.setmode(GPIO.BOARD) # use RaspPi board layout pin numbering
GPIO.setup(pin, GPIO.IN)#, pull_up_down=GPIO.PUD_UP)

counter = 0
tmp_counter = 0

countlock = threading.Lock()

if server:
  client = GraphiteClient(server, port, hostname="newoldpi")
else:
  client = GraphiteClient(hostname="newoldpi")

def tube_impulse_callback(channel): # threaded callback -- falling edge detected
  global counter # make counter global to be able to increment it
  global tmp_counter
  tmp_counter += 1
  if countlock.acquire(False):
     counter += tmp_counter
     tmp_counter = 0
     countlock.release()

# when a falling edge is detected on port 12, regardless of whatever
# else is happening in the program, the tube_impulse_callback will be run
GPIO.add_event_detect(pin, GPIO.FALLING, callback=tube_impulse_callback, bouncetime=10)

try:
  state = "init"
  while True:
    currentMinute = datetime.now().minute
    while datetime.now().minute == currentMinute: # this minute..
      time.sleep(1) # .. wait while add_event_detect detects pulses
    if countlock.acquire():
      if state == "init":
        print "ignoreing initial partial reading"
        state = "running"
      else:
        print "%d cpm" % (counter)
        # tube is a SI29BG
        # so conversion factor is 0.010000 ?
        # might be better to be a bit higher?
        # or maybe my other tube is too high?
        print "%f uSv" % (counter * 0.010000)
        client.poke("raspigeiger.%s.cpm", counter)
        client.poke("raspigeiger.%s.kusv", int((counter * 0.01) * 1000))
      counter=0 # reset counter
      countlock.release()

except KeyboardInterrupt:
  GPIO.cleanup() # clean up GPIO on CTRL+C exit
except:
  e = sys.exc_info()[0]
  print e
  GPIO.cleanup() # clean up GPIO on normal exit

print "exiting"
