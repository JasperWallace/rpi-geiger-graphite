#!/usr/bin/env python
#
#
#

import sys, time, subprocess
from socket import socket, gethostname

class GraphiteClient:
    def __init__(self, server="bogwoppit.lan.pointless.net", port=2003, delay=60, hostname=None):
        self.port = port
        self.server = server
        self.sock = socket()
        self.dryrun = False
        try:
            if not self.dryrun:
                self.sock.connect( (server, port) )
        except:
            print "Couldn't connect to %(server)s on port %(port)d, is carbon-agent.py running?" % { 'server':server, 'port':port }
            sys.exit(1)

        if hostname:
            self.hostname = hostname
        else:
            self.hostname = gethostname()
        self.delay = delay

    def run(self, key, callback):
        while True:
            lines = []
            # Report temperatures
            # e.g. "mything.%s.something"
            mess = key % (self.hostname)
            mess += "%d %d" % (callback(), int(time.time()) )
            print mess
#            lines.append(mess)
            message = '\n'.join(lines) + '\n' #all lines must end in a newline
            print message
            try:
                if not self.dryrun:
                    self.sock.sendall(message)
                else:
                    print message
            except:
                e = sys.exc_info()[0]
                print e
            sys.stdout.flush()
            time.sleep(self.delay)

    def poke(self, key, value):
        lines = []
        mess = key % (self.hostname)
        mess += " %d %d" % (value, int(time.time()) )
        lines.append(mess)
        message = '\n'.join(lines) + '\n' #all lines must end in a newline
        print message
        try:
            if not self.dryrun:
                self.sock.sendall(message)
            else:
                print message
        except:
            e = sys.exc_info()[0]
            print e
        sys.stdout.flush()
